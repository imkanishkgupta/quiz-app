import React, {useState} from 'react';
import Link from "next/link";
import toast from 'react-hot-toast'
import {useRouter} from 'next/router'
import Head from "next/head";
import Heads from "../components/Heads";

const Quiz = () => {
    const router = useRouter()

    const questions = [
        {
            questionText: 'What is the capital of France?',
            answerOptions: [
                {answerText: 'New York', isCorrect: false},
                {answerText: 'London', isCorrect: false},
                {answerText: 'Paris', isCorrect: true},
                {answerText: 'Dublin', isCorrect: false},
            ],
        },

        {
            questionText: 'Who is CEO of Tesla?',
            answerOptions: [
                {answerText: 'Jeff Bezos', isCorrect: false},
                {answerText: 'Elon Musk', isCorrect: true},
                {answerText: 'Bill Gates', isCorrect: false},
                {answerText: 'Tony Stark', isCorrect: false},
            ],
        },
        {
            questionText: 'The iPhone was created by which company?',
            answerOptions: [
                {answerText: 'Apple', isCorrect: true},
                {answerText: 'Intel', isCorrect: false},
                {answerText: 'Amazon', isCorrect: false},
                {answerText: 'Microsoft', isCorrect: false},
            ],
        },
        {
            questionText: 'How many Harry Potter books are there?',
            answerOptions: [
                {answerText: '1', isCorrect: false},
                {answerText: '4', isCorrect: false},
                {answerText: '6', isCorrect: false},
                {answerText: '7', isCorrect: true},
            ],
        },
    ];
    const handleResetButton = () => {
        setCurrentQuestion(0);
        setShowScore(false);
        setScore(0);
    }

    const [auth, setAuth] = useState('');
    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [showScore, setShowScore] = useState(false);
    const [score, setScore] = useState(0);
    const [isAnswered, setIsAnswered] = useState(false);

    const handleAnswerOptionClick = (isCorrect) => {
        if (isCorrect) {
            setScore(score + 1);
            setIsAnswered(true);
            toast.success("Correct Answer !")
        } else {
            toast.error("Incorrect Answer !")
        }

        const nextQuestion = currentQuestion + 1;
        if (nextQuestion < questions.length) {
            setCurrentQuestion(nextQuestion);
        } else {
            setShowScore(true);
        }
    };
    const handleLogOut = () => {
        localStorage.clear();
        router.push('/')
    }
    React.useEffect(() => {
        if (localStorage.getItem('data')) {
            setAuth(true)
        } else {
            setAuth(false)
        }

    }, [])
    return (
        {
            ...auth ? <div className='app'>
                {showScore ? (
                    <>
                        <Heads title="Score"/>
                        <div className='score-section'>
                            <div>You scored {score} out of {questions.length} </div>
                            <button onClick={() => handleResetButton(score)}
                                    className={'flex items-center justify-center'}>Play Again!
                            </button>
                            <button onClick={() => {
                                handleLogOut()
                            }} className={'flex items-center justify-center'}>Log Out
                            </button>
                        </div>
                    </>
                ) : (
                    <>
                        <Heads title="Quiz"/>
                        <div className='question-section'>
                            <div className='question-count'>
                                <span>Question {currentQuestion + 1}</span>/{questions.length}
                            </div>
                            <div className='question-text'>{questions[currentQuestion].questionText}</div>
                        </div>
                        <div className='answer-section'>
                            {questions[currentQuestion].answerOptions.map((answerOption) => (
                                <button
                                    onClick={() => handleAnswerOptionClick(answerOption.isCorrect)}>{answerOption.answerText}</button>
                            ))}
                        </div>

                    </>
                )}
            </div> : <>
                <Heads title="UnAuthenticated"/>
                <div className={"space-y-4"}>
                    <div className={"playballFont tracking-wider"}>UnAuthenticated</div>
                    <Link href='/'>
                        <button>Back To Login</button>
                    </Link>
                </div>
            </>
        }
    );
}
export default Quiz;
