import '../styles/globals.css'
import {Toaster} from 'react-hot-toast'

function MyApp({Component, pageProps}) {
    return (
        <>
            <Component {...pageProps} />
            <div>
                <Toaster position="top-center" duration="4000"/>
            </div>
        </>
    )
}

export default MyApp
