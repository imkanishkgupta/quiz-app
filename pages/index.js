import React, {useState} from "react";
import {useRouter} from 'next/router'
import LoginSvg from "../components/LoginSvg";
import Heads from "../components/Heads";

function Index() {
    // React States
    const [errorMessages, setErrorMessages] = useState({});
    const [isSubmitted, setIsSubmitted] = useState(false);
    const router = useRouter()


    // User Login info
    const database = [
        {
            username: "admin",
            password: "admin",
        },
        {
            username: "kanishk",
            password: "a"
        },
        {
            username: "user2",
            password: "pass2"
        }
    ];

    const errors = {
        uname: "invalid username",
        pass: "invalid password"
    };

    const handleSubmit = (event) => {
        //Prevent page reload
        event.preventDefault();

        let {uname, pass} = document.forms[0];

        // Find user login info
        const userData = database.find((user) => user.username === uname.value);

        // Compare user info
        if (userData) {
            if (userData.password !== pass.value) {
                // Invalid password
                setErrorMessages({name: "pass", message: errors.pass});
            } else {
                setIsSubmitted(true);
                router.push('/quiz')
            }
        } else {
            // Username not found
            setErrorMessages({name: "uname", message: errors.uname});
        }
    };

    React.useEffect(() => {
        if (isSubmitted) {
            localStorage.setItem(
                'data',
                JSON.stringify("admin"),
            )
        }
    }, [isSubmitted])


    // Generate JSX code for error message
    const renderErrorMessage = (name) =>
        name === errorMessages.name && (
            <div className="error">{errorMessages.message}</div>
        );

    // JSX code for login form


    return (
        <>
            <Heads title="Login"/>
            <div>
                <div className={"playballFont text-2xl tracking-wider font-medium"}>
                    Welcome to Quiz App !
                    <span className={"block playballFont my-2"}>
                        Please enter your username and password to continue.
                    </span>
                </div>
                <div className="app flex gap-2">
                    <div className={"flex items-center justify-center"}>
                        <LoginSvg/>
                    </div>
                    <form onSubmit={handleSubmit}>
                        <div className="input-container">
                            <label>Username </label>
                            <input type="text" name="uname" required/>
                            {renderErrorMessage("uname")}
                        </div>
                        <div className="input-container">
                            <label>Password </label>
                            <input type="password" name="pass" required/>
                            {renderErrorMessage("pass")}
                        </div>
                        <div className="button-container">
                            <input type="submit" value={"Login"}/>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
}

export default Index;
